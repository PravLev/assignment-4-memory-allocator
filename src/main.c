#include <stdio.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

size_t count_pass_test = 0;

#define RUN(f){                                                        \
        printf("Test: %s\n", #f);                                      \
        heap_init(0);                                                  \
        char* w = f();                                                 \
        if (w) {                                                       \
            printf("%s -- FAIL: %s\n", #f, w);                         \
        } else {                                                       \
            printf("%s -- SUCCESS\n", #f);                             \
            count_pass_test++;                                         \
                                                                       \
        }                                                              \
    }

static char *test_malloc() {
    void *m = _malloc(64);
    if (m == NULL) {
        return "no memory allocated";
    }
    return NULL;
}

static char *test_one_block_free() {
    void *f = _malloc(0);
    void *s = _malloc(0);
    _free(s);
    struct block_header *block_f = (struct block_header *) (f - offsetof(struct block_header, contents));
    struct block_header *block_s = (struct block_header *) (s - offsetof(struct block_header, contents));
    if (!block_s->is_free) {
        return "the block hasn't been freed";
    }
    if (block_f->next != block_s) {
        return "'next' reference doesn't match";
    }
    return NULL;
}

static char *test_two_blocks_free() {
    void *f = _malloc(0);
    void *s = _malloc(0);
    _free(f);
    _free(s);
    struct block_header *block_f = (struct block_header *) (f - offsetof(struct block_header, contents));
    struct block_header *block_s= (struct block_header *) (s - offsetof(struct block_header, contents));
    if (!block_s->is_free || !block_f->is_free) {
        return "blocks haven't been freed";
    }
    if (block_f->next != block_s) {
        return "'next' reference doesn't match";
    }
    return NULL;
}

static char *test_regions_expansion() {
    _malloc(8000);
    struct block_header *st = HEAP_START;
    struct block_header *exp = (struct block_header *) (_malloc(8000) - offsetof(struct block_header, contents));
    if (st->next != exp) {
        return "new region doesn't extend previous one";
    }
    if (st->capacity.bytes != 8000 || exp->capacity.bytes != 8000) {
        return "wrong capacity";
    }
    return NULL;
}

static char *test_regions_expansion_other_place() {
    struct block_header *start = HEAP_START;
    void *p = mmap(HEAP_START + REGION_MIN_SIZE, 100, 0, MAP_PRIVATE | 0x20, -1, 0);
    _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header *expansion = (struct block_header *)(
                    _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents)) -
                    offsetof(struct block_header, contents));
    munmap(p, 100);
    if (start->next != expansion) {
        return "wrong reference to a new region";
    }
    if (start + offsetof(struct block_header, contents) + start->capacity.bytes == expansion) {
        return "wrong location of a new region";
    }
    return NULL;
}




int main(){
    RUN(test_malloc)
    RUN(test_one_block_free)
    RUN(test_two_blocks_free)
    RUN(test_regions_expansion)
    RUN(test_regions_expansion_other_place)
    return 0;
}